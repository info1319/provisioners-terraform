
# ------------- VARIABLES ----------------------
# vpc_cidr, access_key, secret_key
variable "vpc_cidr" {}
variable "access_key" {}
variable "secret_key" {}
variable "app_prefix" {}
variable "subnet-1-cidr"{}
variable "availability_zone" {}
variable "my_ip" {}
variable "private_key_location" {}

# ----------- PROVIDER -------------
provider "aws" {
  region     = "us-east-1"
}

terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "myapp-bucket-kasper"
    key = "myapp/state.tfstate"
    region = "us-east-1"
  }
}

# Utwórz VPC
resource "aws_vpc" "prod-vpc" {
  cidr_block = var.vpc_cidr
  tags = {
      Name = "${var.app_prefix} - VPC"
  }
}

resource "aws_subnet" "prod-subnet-1" {
  vpc_id = aws_vpc.prod-vpc.id
  cidr_block =  var.subnet-1-cidr
  availability_zone = var.availability_zone
      tags = {
        Name = "${var.app_prefix}-subnet-1"
    }
}

resource "aws_internet_gateway" "prod-igw" {
    vpc_id = aws_vpc.prod-vpc.id
      tags = {
        Name = "${var.app_prefix}-igw"
    }
}

resource "aws_default_route_table" "prod-rtb" {
    default_route_table_id = aws_vpc.prod-vpc.default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.prod-igw.id
    }
    tags = {
        Name = "${var.app_prefix}-main-rtb"
    }
}

resource "aws_security_group" "prod-sg" {
    vpc_id = aws_vpc.prod-vpc.id
    name = "prod-sg"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
        Name = "${var.app_prefix}-sg"
    }
}

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}
output "aws_ami_id" {
    value = data.aws_ami.latest-amazon-linux-image.id
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.prod-subnet-1.id
  
  vpc_security_group_ids = [aws_security_group.prod-sg.id]
  availability_zone = var.availability_zone

  associate_public_ip_address = true
  key_name = "main-key"

  user_data = file("entry-script.sh")
}

